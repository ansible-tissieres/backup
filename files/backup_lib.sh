#!/bin/bash

echo "NEW BACKUP $(date)"

# functions
function mariadb_backup {
    hash mariabackup 2>/dev/null || { echo >&2 "ERROR: mariabackup not installed"; exit 1; }

    mkdir -p "$MARIADB_DIR"
    chmod 700 "$MARIADB_DIR"
    rm -rf "$MARIADB_DIR"/*
    echo "mariabackup --defaults-extra-file=/root/.my.cnf --backup --target-dir=$MARIADB_DIR"
    mariabackup --defaults-extra-file=/root/.my.cnf --backup --target-dir="$MARIADB_DIR" 2>&1
}

function pgsql_backup {
    mkdir -p "$PGSQL_DIR"
    chown postgres:postgres "$PGSQL_DIR"
    chmod 700 "$PGSQL_DIR"
    rm -rf "$PGSQL_DIR"/*
    cd "$PGSQL_DIR"
    dbs=$(sudo -u postgres psql -lt| grep -v : | cut -d \| -f 1 | grep -v template | grep -v -e '^\s*$' | sed -e 's/  *$//')
    for db in $dbs
    do
        echo "sudo -u postgres pg_dump -Fc $db -f $db.pgdump"
        sudo -u postgres pg_dump -Fc "$db" -f "$db.pgdump" 2>&1
    done
    echo "sudo -u postgres pg_dumpall -g -f globals.sql"
    sudo -u postgres pg_dumpall -g -f "globals.sql" 2>&1
}

function rsync_backup {
    RSYNC_PATH="${SSH_USER}@${SSH_HOST}:${RSYNC_DIR}"
    echo "rsync -avhzR -e ssh -p ${SSH_PORT} $@  $RSYNC_PATH"
    rsync -avhzR -e "ssh -p ${SSH_PORT}" "$@"  "$RSYNC_PATH" 2>&1
}

function borg_backup {
    hash borg 2>/dev/null || { echo >&2 "ERROR: borg not installed"; exit 1; }
    REPOSITORY="ssh://${SSH_USER}@${SSH_HOST}:${SSH_PORT}${SSH_DIR}"

    BORG_PASSPHRASE="${PASSPHRASE}" borg init -e repokey "${REPOSITORY}" 2>&1
    echo "borg create -sv -C lzma ${REPOSITORY}::$(hostname)-$(date +%Y-%m-%d) $@"
    BORG_PASSPHRASE="${PASSPHRASE}" borg create -sv -C lzma "${REPOSITORY}::$(hostname)-$(date +%Y-%m-%d)" "$@" 2>&1
    BORG_PASSPHRASE="${PASSPHRASE}" borg prune -sv --keep-within 3m "${REPOSITORY}" 2>&1

}

function end {
    unset PASSPHRASE
    exit 0
}
